$(window).on('load', function () {

    $('.slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

    var phone = document.querySelectorAll('.phonemask');
    phone.forEach(function (element) {
        var phoneMask = IMask(
            element, {
                mask: '+{7}(000)-000-00-00',
                lazy: true,
                overwrite: false,
            });
    });

});