if($('#map').length){
    var map = document.querySelector('#map');

var onIntersection = function (entries) {
    for (const entry of entries) {
        if (entry.intersectionRatio > 0) {
            ymaps.ready(mapsInit);
            observer.unobserve(entry.target);
        }
    }
};

var observer = new IntersectionObserver(onIntersection);
observer.observe(map);

function mapsInit() {
    myMap = new ymaps.Map('map', {
        center: ($(window).width() <= '768') ? [59.9386300, 30.3141300] : [59.9386300, 30.3141300],
        zoom: 13,
        controls: ['zoomControl']
    });
    var Piter = new ymaps.Placemark([59.9386300, 30.3141300], {
        balloonContent: ''
    }, {
        hasBalloon: false,
        iconLayout: 'default#image',
        iconImageHref: '/img/Subtract.svg',
        iconImageSize: [150, 180.77],
        iconImageOffset: [-4, -77]
    });
    myMap.geoObjects.add(Piter);
    myMap.behaviors.disable('scrollZoom');
};

}
